package com.utils;

import java.util.HashMap;
import java.util.Map;

import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;

public class LuaUtil {

	public static Map<String, String> luaTable2Map(LuaTable criteriasMap) {
		Map<String, String> map = new HashMap<>();
		LuaValue[] keys = criteriasMap.keys();
		for (LuaValue key : keys) {
			map.put(key.toString(), criteriasMap.get(key).toString());
		}
		return map;
	}

	public static String[] luaTable2Ary(LuaTable showFields) {
		LuaValue[] keys = showFields.keys();
		String[] showFieldsAry = new String[keys.length];
		for (int i = 1; i <= keys.length; i++) {
			showFieldsAry[i - 1] = showFields.get(i).toString();
		}
		return showFieldsAry;
	}
}
