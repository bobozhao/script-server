package com.utils;

import java.util.Map;
import java.util.Map.Entry;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.alibaba.fastjson.JSONObject;
import com.mongodb.BasicDBObject;

import lombok.extern.slf4j.Slf4j;

/**
 * 简单的mongo工具类封装,只提供"="条件的匹配,其他条件的匹配自行继承实现
 * 
 * @author ZGame
 *
 *
 */
@Slf4j
public class SimpleDBUtil {

	private final static String playerCollection = "player";

	/**
	 * 查询集合中是否存在指定条件的数据
	 * 
	 * @param mongoTemplate
	 * @param collectionName
	 * @param criterias
	 * @return
	 */
	public static boolean exists(MongoTemplate mongoTemplate, String collectionName, String... criterias) {
		if (criterias.length < 2) {
			log.info("格式错误,条件长度小于2");
			return false;
		}
		if (criterias.length % 2 != 0) {
			log.info("格式错误,条件必须为偶数对应");
			return false;
		}
		BasicDBObject dbObject = new BasicDBObject();
		for (int i = 0; i < criterias.length; i += 2) {
			dbObject.put(criterias[i], criterias[i + 1]);
		}
		Query query = new BasicQuery(dbObject.toJson());
		return mongoTemplate.exists(query, collectionName);
	}

	/**
	 * 查询是否存在玩家
	 * 
	 * @param mongoTemplate
	 * @param userId
	 * @return
	 */
	public static boolean existsPlayer(MongoTemplate mongoTemplate, String userId) {
		return mongoTemplate.exists(new Query(Criteria.where("userId").is(userId)), playerCollection);
	}

	/**
	 * 查询 玩家快照
	 * 
	 * @param mongoTemplate
	 * @param userId
	 * @param criterias
	 * @return
	 */
	public static JSONObject findPlayerSnap(MongoTemplate mongoTemplate, Map<String,String> criteriasMap, String... showFields) {
		BasicDBObject dbObject = new BasicDBObject();
		for(Entry<String, String> entry : criteriasMap.entrySet()){
			String key = entry.getKey();
			String value = entry.getValue();
			dbObject.put(key, value);
		}
		BasicDBObject dbObjectShow = new BasicDBObject();
		for(String show:showFields){
			dbObjectShow.put(show, true);
		}
		Query query = new BasicQuery(dbObject.toJson(), dbObjectShow.toJson());
		return mongoTemplate.findOne(query, JSONObject.class, playerCollection);
	}

	/**
	 * 查询其他集合快照
	 * 
	 * @param mongoTemplate
	 * @param userId
	 * @param criterias
	 * @return
	 */
	public static JSONObject findOtherSnap(MongoTemplate mongoTemplate, String userId, String collectionName,
			String... criterias) {
		BasicDBObject dbObject = new BasicDBObject();
		dbObject.put("userId", userId);
		BasicDBObject dbObjectShow = new BasicDBObject();
		for (String criteria : criterias) {
			dbObjectShow.put(criteria, true);
		}
		Query query = new BasicQuery(dbObject.toJson(), dbObjectShow.toJson());
		return mongoTemplate.findOne(query, JSONObject.class, collectionName);
	}

	/**
	 * 根据传入的class查找对应集合的整条完整数据
	 * 
	 * @param mongoTemplate
	 * @param userId
	 * @param clazz
	 * @return
	 */
	public static <T> T findDataByUserId(MongoTemplate mongoTemplate, String userId, Class<T> clazz) {
		return mongoTemplate.findOne(new Query(Criteria.where("userId").is(userId)), clazz);
	}
}
