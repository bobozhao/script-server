package com.utils;

import java.io.File;
import java.util.List;

public class FileUtil {

	public static List<File> listDirectory(File dir,List<File> fileList) {
		if (!dir.exists()) {
			throw new IllegalArgumentException("目录" + dir + "不存在");
		}
		if (!dir.isDirectory()) {
			throw new IllegalArgumentException(dir + "不是目录");
		}
		File[] files = dir.listFiles();
		if (files != null && files.length > 0) {
			for (File file : files) {
				if (file.isDirectory()) {
					listDirectory(file,fileList);
				} else {
					fileList.add(file);
				}
			}
		}
		return fileList;
	}
}
