package com.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.manager.JavaScriptManager;

@RestController
public class ScriptController {

	@Autowired
	private JavaScriptManager javaScriptManager;

	
	@GetMapping("/executeScript")
	private String executeScript(@RequestBody(required = false) String bodyContent,HttpServletRequest request,String scriptName) {
		Map<String, String[]> parameterMap = request.getParameterMap();
		return javaScriptManager.excuteScript(scriptName,parameterMap,bodyContent);
	}
}
