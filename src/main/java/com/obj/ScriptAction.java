package com.obj;


import com.alibaba.fastjson.JSONObject;

public interface ScriptAction {

	public JSONObject findPlayerSnap(Object arg1, Object arg2);
	public void savePlayer(String playerData);
}
