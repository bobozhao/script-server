package com.obj;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.script.CompiledScript;
import javax.script.ScriptEngine;

import org.luaj.vm2.LuaTable;
import org.slf4j.Logger;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.alibaba.fastjson.JSONObject;
import com.utils.LuaUtil;
import com.utils.SimpleDBUtil;

public class LuaScriptContext extends AbstractScriptContext implements ScriptAction {

	public LuaScriptContext(MongoTemplate mongoTemplate, CompiledScript compiledScript, ScriptEngine engine,Logger console) {
		super(mongoTemplate, compiledScript, engine,console);
	}
	@Override
	public void setParam(Map<String, String[]> parameterMap) {
		LuaTable luaValue = new LuaTable();
		Iterator<Entry<String, String[]>> itr = parameterMap.entrySet().iterator();
		while (itr.hasNext()) {
			Entry<String, String[]> entry = itr.next();
			String key = entry.getKey();
			String[] values = entry.getValue();
			LuaTable luaAryValue = new LuaTable();
			for (int i = 1; i <= values.length; i++) {
				luaAryValue.set(i, values[i-1]);
			}
			luaValue.set(key, luaAryValue);
		}
		this.param = luaValue;
	}
	
	@Override
	public JSONObject findPlayerSnap(Object arg1, Object arg2) {
		LuaTable criteriasMap = (LuaTable) arg1;
		LuaTable showFields = (LuaTable) arg2;
		return SimpleDBUtil.findPlayerSnap(playermongoTemplate, LuaUtil.luaTable2Map(criteriasMap),
				LuaUtil.luaTable2Ary(showFields));
	}
	@Override
	public void savePlayer(String playerData) {
		
	}
}
