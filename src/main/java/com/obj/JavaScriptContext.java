package com.obj;

import java.util.Map;

import javax.script.CompiledScript;
import javax.script.ScriptEngine;

import org.slf4j.Logger;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.alibaba.fastjson.JSONObject;
import com.utils.SimpleDBUtil;

public class JavaScriptContext extends AbstractScriptContext implements ScriptAction {
	public Map<String, String[]> parameterMap;

	public JavaScriptContext(MongoTemplate mongoTemplate, CompiledScript compiledScript, ScriptEngine engine,
			Logger console) {
		super(mongoTemplate, compiledScript, engine,console);
	}

	@Override
	public void setParam(Map<String, String[]> parameterMap) {
		this.param = parameterMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject findPlayerSnap(Object arg1, Object arg2) {
		Map<String, String> criteriasMap = JSONObject.parseObject(JSONObject.toJSONString(arg1), Map.class);
		Map<String, String> showFieldsMap = JSONObject.parseObject(JSONObject.toJSONString(arg2), Map.class);
		String[] showFields = new String[showFieldsMap.size()];
		showFields = showFieldsMap.values().toArray(showFields);
		return SimpleDBUtil.findPlayerSnap(playermongoTemplate, criteriasMap, showFields);
	}

	@Override
	public void savePlayer(String playerData) {

	}
}
