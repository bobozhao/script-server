package com.obj;

import java.util.Map;

import javax.script.CompiledScript;
import javax.script.ScriptEngine;

import org.slf4j.Logger;
import org.springframework.data.mongodb.core.MongoTemplate;

import lombok.Getter;

@Getter
public abstract class AbstractScriptContext {

	public MongoTemplate playermongoTemplate;
	public CompiledScript compiledScript;
	public ScriptEngine engine;
	public Object param;
	public Object body;
	public Logger console;

	public AbstractScriptContext(MongoTemplate playermongoTemplate, CompiledScript compiledScript, ScriptEngine engine,
			Logger console) {
		super();
		this.playermongoTemplate = playermongoTemplate;
		this.compiledScript = compiledScript;
		this.engine = engine;
		this.console = console;
	}

	public abstract void setParam(Map<String, String[]> parameterMap);
}
