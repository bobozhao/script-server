
/**
 * js demo
 * @param serverContext
 * 把服务器的控制权全部放在serverContext里面,让写脚本的开发人员自由的使用这些对象进行逻辑开发
 * serverContext包含提供操作所用的对象:
 * [logger,requestParams,requestBody]
 * 实现了操作行为ScriptAction:
 * findPlayerSnap(Object arg1, Object arg2)
 * savePlayer(String playerData)
 * @returns
 */
function findPlayerSnap(serverContext){
	//serverContext提供的可操作对象
	var console = serverContext.console//logger
	var parameterMap = serverContext.param//serverContext获取所有参数map
	var requestBody = serverContext.body//requestBody
	
	
	//获取参数
	var pid = parameterMap.playerId[0]
	var criteriasMap = {_id:pid}//创建数据库查询条件
	var showFiled =["playerClientData.pveSid"]//创建查询结果显示字段
	
	
	//执行action
	var result = serverContext.findPlayerSnap(criteriasMap,showFiled)//开始查询
	
	console.info("excute js right")
	
	
	//action 结果
	var pveSid = result.playerClientData.pveSid
	
	//返回值
	return pveSid;
}
findPlayerSnap(serverContext)


